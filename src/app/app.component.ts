import { Component, Inject } from '@angular/core';
import {Http} from '@angular/http';
import {trigger,state,animate,transition,style} from '@angular/animations'

 var ani =[
   trigger("tri1",[state("vis",style({top:"150px"})),
   state("invis",style({top:"-1000px"})),
   transition("vis<=>invis",animate("1000ms"))
  ]),
  trigger("tri2", [state("vis", style({ top: "230px" })),
  state("invis", style({ top: "-1000px" })),
  transition("vis<=>invis", animate("1000ms"))
  ])

 ];


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:[ani]
})
export class AppComponent {

  constructor(@Inject(Http) public obj){}

  temp="invis";reg="invis";req;
  rid;rpwd;rrepwd;rmob;lid;pwd;cdata;newa;




  ///////////logic from///////

  funlogin() {

    var data = { email: this.lid, password: this.pwd }

    this.obj.post("catfile/login", data).subscribe(dt => {
      var cdata = dt._body.length

      if (cdata != 0) {
        var udata = JSON.parse(dt._body)

        if (udata.length == 1) {
          localStorage.setItem("userlogin",'1')
          this.temp = "invis"
          this.lid = "";
          this.pwd = "";
          this.ngOnInit();

        }

      } else {
        alert("enter valid details")
      }


    })
  }


  funloginform(){
   // alert("login click")
    this.temp="vis"
  }

  funclose(){
    this.temp="invis"
  }

  funregclose(){
    alert("reg")
    this.reg="invis"
  }

  funregform(){
    this.reg="vis"
    this.temp="invis"
  }

  funreg(){
   // var email=this.rid
    var data={email:this.rid,password:this.rpwd,rpassword:this.rrepwd,mobile:this.rmob}
        console.log(data,"userdata")
        this.obj.post("catfile/reg",data).subscribe(dt=>{
          
        })
        this.reg="invis"
  }
  logout(){
    localStorage.removeItem("userlogin")
    this.ngOnInit()
  }
 



  ngOnInit(){
if(localStorage.getItem("userlogin")){
  this.newa=1
}else{
  this.newa=0
}

  }

}

