import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
//import {FontAwesomeModule} from '@fortawesome/angular-fontawesome'
import{ FormsModule} from '@angular/forms'
import {RouterModule}from '@angular/router'
import { AppComponent } from './app.component';
import { MenComponent } from './men/men.component';
import { WomenComponent } from './women/women.component';
import { ElectronicsComponent } from './electronics/electronics.component';
import { SportsComponent } from './sports/sports.component';
import { HomeComponent } from './home/home.component';


var  obj=[{path:"",component:HomeComponent},{path:"men",component:MenComponent},{path:"women",component:WomenComponent},{path:"electronics",component:ElectronicsComponent},{path:"sports",component:SportsComponent},{path:"home",component:HomeComponent}]
var router =RouterModule.forRoot(obj)



@NgModule({
  declarations: [
    AppComponent,
    MenComponent,
    WomenComponent,
    ElectronicsComponent,
    SportsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,router,HttpModule,FormsModule,BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
